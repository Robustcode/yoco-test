import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  private crossIcon: string;
  private checkMarkIcon: string;
  public commaIcon: string;
  public commaStatus: boolean = false;
  public input: string;
  public total: string;
  public registerList: any[] = [];
  public calculatorButtonList: any = [];

  constructor() { }

  ngOnInit(): void {
    this.input = '0.00';
    this.total = '0';
    this.crossIcon = `<img src="assets/icons/cross.png" />`;
    this.checkMarkIcon = `<img src="assets/icons/check-mark.png" />`;
    this.commaIcon = `<strong>.</strong>`;
    /* I decided to use an array to generate the keypad as it would 
    be quicker than repeatedly copying and editing the items... */
    this.calculatorButtonList = [
      ['1', '2', '3'],
      ['4', '5', '6'],
      ['7', '8', '9'],
      [
        this.crossIcon,
        '0',
        this.checkMarkIcon,
        this.commaIcon
      ]
    ];
  }

  private addNumber(number: number): void {
    const TEMP_VALUE = this.convertToDecimal(number);
    if (parseFloat(TEMP_VALUE) < 1000000) {
      if (this.input?.indexOf('.') > -1 && this.input?.slice(-1) != '.') {
        this.input = TEMP_VALUE;
      } else {
        this.input = parseFloat(TEMP_VALUE)?.toFixed(2);
      }
    }
  }

  private removeNumber(): void {
    if (parseFloat(this.input) !== 0 && this.input?.indexOf('.') > -1) {
      let rands: string = this.input?.split('.')[0];
      let cents: string = this.input?.split('.')?.pop();

      if (!!this.commaStatus) {
        cents = `${(this.input?.split('.')?.pop())?.slice(0, -1)}`;
      } else {
        rands = `${(this.input?.split('.')[0])?.slice(0, -1)}`;
      }

      this.input = `${!!rands ? rands : '0'}.${!!cents ? cents : '00'}`;
    }
  }

  private addItem(): void {
    if (parseFloat(this.input) > 0) {
      this.registerList?.push(this.input);
    }

    this.input = '0.00';
    this.commaStatus = false;
    this.setTotal();
  }

  private setTotal(): void {
    this.total = this.registerList?.reduce((a, b) => parseFloat(a) + parseFloat(b), 0);
  }

  private convertToDecimal(newValue: number): string {
    /* Here is most of the code that makes the comma functionality work */
    if (this.input?.indexOf('.') > -1) {
      let rands: string = this.input?.split('.')[0];
      let cents: string = this.input?.split('.')?.pop();

      if (!!this.commaStatus) {
        const TEST_CENTS = !!this.input?.split('.')?.pop() && parseInt(this.input?.split('.').pop()) > 0;
        cents = `${TEST_CENTS ? this.input?.split('.')?.pop() : ''}${newValue}`;
      } else {
        const TEST_RANDS = !!this.input?.split('.')[0] && parseInt(this.input?.split('.')[0]) > 0;
        rands = `${TEST_RANDS ? this.input?.split('.')[0] : ''}${newValue}`;
      }

      return `${rands}.${cents}`;
    }
  }

  public trigger(value: string): void {
    /* Here is where i am identifyting the item that was clicked */
    if (value === this.crossIcon) {
      this.removeNumber();
    } else if (value === this.checkMarkIcon) {
      this.addItem();
    } else if (value === this.commaIcon) {
      this.commaStatus = !this.commaStatus;
    } else {
      this.addNumber(parseFloat(value));
    }
  }

  public removeItem(number: number): void {
    const INDEX = this.registerList?.indexOf(number);

    if (INDEX > -1) {
      this.registerList?.splice(INDEX, 1);
      this.setTotal();
    }
  }

}
